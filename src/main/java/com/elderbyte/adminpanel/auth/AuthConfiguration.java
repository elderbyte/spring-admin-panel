package com.elderbyte.adminpanel.auth;

import com.elderbyte.warden.spring.auth.server.WardenAuthService;
import de.codecentric.boot.admin.server.web.client.HttpHeadersProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AuthConfiguration {

    private final Logger log = LoggerFactory.getLogger(getClass());


    @Value("${client.actuator.token}")
    private String actuatorUserToken;

    @Bean
    public HttpHeadersProvider httpHeadersProvider(WardenAuthService wardenAuthService){

        log.info("Configuring custom HttpHeadersProvider to inject bearer tokens!");
        return new WardenHttpHeaderProvider(actuatorUserToken, wardenAuthService);
    }
}
