package com.elderbyte.adminpanel.auth;

import com.elderbyte.warden.spring.auth.authentication.oauth.OAuthTokenDto;
import com.elderbyte.warden.spring.auth.server.WardenAuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


public class WardenHttpHeaderProvider extends BearerTokenHttpHeadersProvider{

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final WardenAuthService wardenClientService;
    private final String actuatorUserToken;


    public WardenHttpHeaderProvider(String actuatorUserToken, WardenAuthService wardenClientService){

        if(actuatorUserToken == null) throw new IllegalArgumentException("actuatorUserToken was null!");
        if(wardenClientService == null) throw new IllegalArgumentException("wardenClientService was null!");


        this.wardenClientService = wardenClientService;
        this.actuatorUserToken = actuatorUserToken;
    }

    @Override
    protected Optional<String> getBearerToken() {

        try {
            OAuthTokenDto jwt = wardenClientService.getJwtForUserToken(actuatorUserToken)
                    .get(1000, TimeUnit.MILLISECONDS);

            return Optional.of(jwt.toBearerToken());

        } catch (TimeoutException |ExecutionException|InterruptedException e) {
            log.warn("Failed to fetch JWT for user token!", e);
        }
        return Optional.empty();
    }
}
