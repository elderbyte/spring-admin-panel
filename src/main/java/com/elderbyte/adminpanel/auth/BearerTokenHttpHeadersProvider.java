package com.elderbyte.adminpanel.auth;


import de.codecentric.boot.admin.server.domain.entities.Instance;
import de.codecentric.boot.admin.server.web.client.HttpHeadersProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;

import java.util.Optional;


public abstract class BearerTokenHttpHeadersProvider implements HttpHeadersProvider {

    private final Logger log = LoggerFactory.getLogger(getClass());



    @Override
    public HttpHeaders getHeaders(Instance application) {
        HttpHeaders headers = new HttpHeaders();

        getBearerToken().ifPresent(token -> {
            headers.set(HttpHeaders.AUTHORIZATION, token);
            log.trace("Injecting bearer token " + token);
        });

        return headers;
    }
    protected abstract Optional<String> getBearerToken();
}
